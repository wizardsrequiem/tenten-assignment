package com.richard.assignment.presentation.computer;

import android.support.annotation.NonNull;

import com.richard.assignment.presentation.computer.domain.model.Instruction;
import com.richard.assignment.util.AddressOutOfBoundsException;

/**
 * Created by Richard on 8/22/2017.
 */

public class ComputerPresenter implements ComputerContract.Presenter, OutputListener {

    private static final int PRINT_TENTEN_BEGIN = 50;
    private static final int MAIN_BEGIN = 0;
    private ComputerContract.View view;
    private ComputerSimulatorImpl computer;

    public ComputerPresenter(@NonNull ComputerContract.View view){

        this.view = view;

        this.view.setPresenter(this);
    }

    @Override
    public void start() {
        startComputerSimulator();
    }

    //inserts the instructions to the InstructionSet Object
    @Override
    public void startComputerSimulator() {

        try {
            computer = new ComputerSimulatorImpl(100);
            computer.setAddress(PRINT_TENTEN_BEGIN);
            computer.insert(new Instruction(Command.MULT, null));
            computer.insert(new Instruction(Command.PRINT, null));
            computer.insert(new Instruction(Command.RET, null));
            computer.setAddress(MAIN_BEGIN);
            computer.insert(new Instruction(Command.PUSH, 1000));
            computer.insert(new Instruction(Command.PUSH, 10));
            computer.insert(new Instruction(Command.ADD, null));
            computer.insert(new Instruction(Command.PRINT));
            computer.insert(new Instruction(Command.PUSH, 9));
            computer.insert(new Instruction(Command.PUSH, 9));
            computer.insert(new Instruction(Command.PUSH, 101));
            computer.insert(new Instruction(Command.PUSH, 10));
            computer.insert(new Instruction(Command.CALL, PRINT_TENTEN_BEGIN));
            computer.insert(new Instruction(Command.STOP, null));
            computer.setAddress(MAIN_BEGIN);

            //executes the stored instructions
            computer.execute(this);
//            view.showPrintedMessage(computer.getOutput());
        }
        catch(NullPointerException e){
            e.printStackTrace();
            view.showShortMessage("The Program has encountered an error.");
        }
        catch(AddressOutOfBoundsException e){
            view.showShortMessage(e.getMessage());
        }

    }

    @Override
    public void printOutput(String output) {
        view.showPrintedMessage(output);
    }
}
