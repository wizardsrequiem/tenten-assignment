package com.richard.assignment.presentation.computer;

import com.richard.assignment.presentation.computer.domain.model.Instruction;
import com.richard.assignment.util.AddressOutOfBoundsException;

/**
 * Created by Richard on 8/21/2017.
 */

public interface ComputerSimulator {

    void setAddress(Integer value);

    void insert(Instruction instuction) throws AddressOutOfBoundsException;

    void execute(OutputListener listener);

    String getOutput();

}
