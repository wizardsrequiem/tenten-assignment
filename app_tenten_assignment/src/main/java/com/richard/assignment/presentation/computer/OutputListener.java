package com.richard.assignment.presentation.computer;

/**
 * Created by Richard on 8/22/2017.
 */

public interface OutputListener {

    void printOutput(String output);

}
