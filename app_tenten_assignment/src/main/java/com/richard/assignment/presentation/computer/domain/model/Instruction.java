package com.richard.assignment.presentation.computer.domain.model;

import com.richard.assignment.presentation.computer.Command;

/**
 * Created by Richard on 8/21/2017.
 */

public class Instruction {

    private Command key;
    private Integer value;

    public Instruction(Command key) {
        this(key, null);
    }

    public Instruction(Command key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public Command getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }
}
