package com.richard.assignment.presentation.computer;

import com.richard.assignment.BasePresenter;
import com.richard.assignment.BaseView;

/**
 * Created by Richard on 8/21/2017.
 */

public class ComputerContract {

    public interface View extends BaseView<Presenter> {

        void showPrintedMessage(String message);
        void showShortMessage(int message);
        void showShortMessage(String message);
        void finish();
    }

    public interface Presenter extends BasePresenter {

        void startComputerSimulator();

    }

}
