package com.richard.assignment.presentation.computer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.richard.assignment.BaseFragment;

import mobile.eteeap.com.app_tenten_assignment.R;

/**
 * Created by Richard on 8/21/2017.
 */

public class ComputerFragment extends BaseFragment implements ComputerContract.View{

    private ComputerContract.Presenter presenter;
    private TextView tv_output;

    public static ComputerFragment newInstance() {
        return new ComputerFragment();
    }

    @Override
    public void setPresenter(ComputerContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.computer_frag, container, false);
        init(view);
        return view;
    }

    private void init(View view){
        tv_output = (TextView) view.findViewById(R.id.tv_output);
    }

    @Override
    public void showPrintedMessage(String message) {
        tv_output.append(message+"\n");
    }

    @Override
    public void showShortMessage(int message){
        showMessage(message);
    }

    @Override
    public void showShortMessage(String message) {
        showMessage(message);
    }

    @Override
    public void finish(){
        getActivity().finish();
    }
}
