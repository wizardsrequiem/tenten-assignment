package com.richard.assignment.presentation.computer;

import com.richard.assignment.presentation.computer.domain.model.Instruction;
import com.richard.assignment.util.AddressOutOfBoundsException;

import java.util.Stack;

/**
 * Created by Richard on 8/21/2017.
 */

public class ComputerSimulatorImpl implements ComputerSimulator{

    private Integer currentAddress;
    private InstructionSet instructionSet;
    private Stack<Integer> computerStack;
    private boolean terminateProgram = false;
    private StringBuilder finalOutput;


    public ComputerSimulatorImpl(Integer capacity) {
        this.currentAddress = 0;
        this.instructionSet = new InstructionSet(capacity);
        this.computerStack = new Stack();
        this.finalOutput = new StringBuilder();
    }

    @Override
    public void setAddress(Integer address) {
        this.currentAddress = address;
    }

    @Override
    public void insert(Instruction instruction) throws NullPointerException, AddressOutOfBoundsException{
        System.out.println("size="+instructionSet.getSize()+" address="+currentAddress+", Key="+instruction.getKey());
        instructionSet.put(currentAddress, instruction);
        currentAddress++;
    }

    @Override
    public void execute(OutputListener listener) throws NullPointerException{
        while(!terminateProgram){
            Instruction instruction = instructionSet.get(currentAddress);
            if(instruction!=null)
                commandHandler(instruction, listener);
            else
                commandHandler(new Instruction(Command.STOP, null), listener);
        }
    }

    @Override
    public String getOutput() {
        if(finalOutput!=null)
            return finalOutput.toString();
        return "No Output";
    }

    private void commandHandler(Instruction instruction, OutputListener listener) throws NullPointerException{

        switch(instruction.getKey()){
            default:
                currentAddress++;
                break;
            case PRINT:
                String output = String.valueOf(computerStack.pop());
                System.out.println(output);
                finalOutput.append(output+"\n");
                listener.printOutput(output);
                currentAddress++;
                break;
            case PUSH:
                computerStack.push(instruction.getValue());
                currentAddress++;
                break;
            case MULT:
                Integer multValue = computerStack.pop();
                computerStack.push(multValue*computerStack.pop());
                currentAddress++;
                break;
            case ADD:
                Integer addValue = computerStack.pop();
                computerStack.push(addValue+computerStack.pop());
                currentAddress++;
                break;
            case CALL:
                currentAddress = instruction.getValue();
                break;
            case RET:
                currentAddress = computerStack.pop();
                break;
            case STOP:
                finalOutput.append("PROGRAM TERMINATED.\n");
                listener.printOutput("PROGRAM TERMINATED.\n");
                terminateProgram = true;
                break;
        }
    }

}
