package com.richard.assignment.presentation.computer;

import com.richard.assignment.presentation.computer.domain.model.Instruction;
import com.richard.assignment.util.AddressOutOfBoundsException;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * Created by Richard on 8/21/2017.
 */

public class InstructionSet implements Iterable<Instruction>{

    private int capacity;
    private TreeMap<Integer, Instruction> treeMap;

    public InstructionSet(int size) {
        super();
        this.capacity = size;
        this.treeMap = new TreeMap<Integer, Instruction>();
    }

    public void put(int index, Instruction instruction) throws NullPointerException, AddressOutOfBoundsException {
        if(!(index > capacity)){
            treeMap.put(index, instruction);
            return;
        }
        throw new AddressOutOfBoundsException("Instruction address is greater than the capacity.");
    }

    public Instruction get(int index){
        return treeMap.get(index);
    }

    public int getSize(){
        return treeMap.size();
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public Iterator iterator() {
		/*
	      get Collection of values contained in TreeMap using
	      Collection values()
	    */
        Collection c = treeMap.values();

        //obtain an Iterator for Collection
        Iterator itr = c.iterator();

        return itr;
    }
}
