package com.richard.assignment.presentation.computer;

/**
 * Created by Richard on 8/21/2017.
 */

public enum Command {
    PUSH, PRINT, MULT, CALL, RET, STOP, ADD
}
