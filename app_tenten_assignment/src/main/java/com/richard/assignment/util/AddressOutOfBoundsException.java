package com.richard.assignment.util;

/**
 * Created by Richard on 8/22/2017.
 */

public class AddressOutOfBoundsException extends Exception{

    public AddressOutOfBoundsException(String message) {
        super(message);
    }

    public AddressOutOfBoundsException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
