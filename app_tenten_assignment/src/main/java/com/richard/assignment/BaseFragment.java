package com.richard.assignment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;

/**
 * Created with Android Studio
 *
 * @author: Richard Mendoza
 * @date: 01/10/2017.
 */

public class BaseFragment extends Fragment {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void showMessage(int message) {
        try {
            if (getView() != null)
                Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    public void showMessage(String message) {
        try {
            if (getView() != null)
                Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
